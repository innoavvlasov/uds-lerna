const mysql = require('mysql2/promise');

class DataBase {
    constructor() {
        this.connection = null;
    }

    async init() {
        try {
            this.connection = await mysql.createConnection({
                host: "localhost",
                user: "UDSUSER",
                port: "3307",
                password: "udsdeveloper"
            });
        } catch (error) {
            throw 'Connection error'
        }
    }

    end() {
        this.connection.end();
    }

    execute(...params) {
        return this.connection.execute(...params);
    }
}

module.exports = { DataBase };