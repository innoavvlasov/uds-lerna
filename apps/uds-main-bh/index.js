const express = require('express');

const app = express();

if(process.env.BD) {
    app.use('/main', require('./main/main.controller'));
} else {
    app.use(require('./api'));
}


app.listen(8090, () => console.log("Listening on port 8090!"));